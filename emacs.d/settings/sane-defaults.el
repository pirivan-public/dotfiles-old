;; Use Shift+Arrow to navigate windows (panes)
(windmove-default-keybindings)

;; Initialize the exec-path-from-shell package
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;; Don't display banner page on startup
(setq inhibit-startup-message t)

;; Don't display the menu bar
(menu-bar-mode 0)

;; Don't display the tool bar
(tool-bar-mode 0)

;; Always display line and column numbers
(setq line-number-mode t)
(setq column-number-mode t)

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; Lines should be 80 characters wide, not 72
(set-default 'fill-column 80)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)
(setq indent-line-function 'insert-tab)
(setq-default tab-width 4)

;; Show empty lines at end of buffer
(set-default 'indicate-empty-lines t)

;; Auto-fill-mode
(defun auto-fill-mode-on () (auto-fill-mode 1))
(add-hook 'text-mode-hook 'auto-fill-mode-on)
(add-hook 'emacs-lisp-mode 'auto-fill-mode-on)
(add-hook 'tex-mode-hook 'auto-fill-mode-on)
(add-hook 'latex-mode-hook 'auto-fill-mode-on)
(add-hook 'org-mode-hook 'auto-fill-mode-on)

;; Highlight current line
(global-hl-line-mode 1)

;; Use smartparens in all buffers
(smartparens-global-mode t)

;; Use CUA mode by default
(cua-mode t)

;; Use the material theme
(load-theme 'material t) 

;; Enable on-the-fly spell checking for text files
(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

;; Use Gnome's default web browser
(setq browse-url-browser-function 'browse-url-generic
browse-url-generic-program "xdg-open")

;; set ssh as the default protocol for tramp
(setq tramp-default-method "ssh")

;; use pandoc to render markdown documents
(setq markdown-command "/usr/bin/pandoc")

(provide 'sane-defaults)
