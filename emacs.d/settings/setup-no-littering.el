(require 'no-littering)

;; Use no-littering for auto-save files
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(provide 'setup-no-littering)
