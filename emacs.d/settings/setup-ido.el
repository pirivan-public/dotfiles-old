;; Interactively Do Things
(require 'ido)
(ido-mode t)
(ido-everywhere t)
(setq ido-enable-flex-matching t
      ido-max-prospects 10)

(require 'ido-completing-read+)
(ido-ubiquitous-mode 1)

(provide 'setup-ido)

