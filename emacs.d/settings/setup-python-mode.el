;; The following Python packages are to be installed:
;; sudo pip3 install jedi flake8 ipython
;; https://realpython.com/blog/python/emacs-the-best-python-editor/

(elpy-enable)

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

(provide 'setup-python-mode)
