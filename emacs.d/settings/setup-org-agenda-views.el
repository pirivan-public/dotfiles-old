;; custom agenda views
(setq org-agenda-custom-commands
      '(("r" "Items to Refile"
         ((tags "+@refile"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags t))
         )
        ("nh" "Notes from Home"
         ((tags "+@home+draft")
          (tags "+@home+final"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags nil))
         )
        ("nw" "Notes from Work"
         ((tags "+@work+draft")
          (tags "+@work+final"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags nil))
         )
        ("nW" "Notes from Workstation"
         ((tags "+@workstation+draft")
          (tags "+@workstation+final"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags nil))
         )
        ("nth" "Tasks from Home"
         ((tags-todo "+@home"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags t))
         )
        ("ntw" "Tasks from Work"
         ((tags-todo "+@work"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags t))
         )
        ("ntW" "Tasks from Workstation"
         ((tags-todo "+@workstation"))
         ((org-agenda-prefix-format " ")
          (org-agenda-remove-tags t))
         )
        ))
