;; Edit file with sudo
(global-set-key (kbd "M-s e") 'sudo-edit)

;; Increase/decrease text size
;; C-x C-0 restores the default font size
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

(provide 'key-bindings)
