This repository hosts some of the Linux dot files in my home directory.

* [tmux](https://github.com/tmux/tmux) configuration file.

* [Emacs](https://www.gnu.org/software/emacs/) configuration files.

* [Visual Studio Code](https://code.visualstudio.com/) configuration changes.

# Installation

1. Clone this repository somewhere in your home directory.

    ```bash
    mkdir ~/Workspace
    cd ~/Workspace
    git clone https://gitlab.com/pirivan/dotfiles.git
    ```

1. Set up bash.

    ```bash
    cd ~
    ln -s ~/Workspace/dotfiles/bash_profile .bash_profile
    ```

1. Set up zsh. You must install [oh-my-zsh](https://github.com/ohmyzsh)

    ```bash
    cd ~
    ln -s ~/Workspace/dotfiles/zshrc .zshrc
    ln -s ~/Workspace/dotfiles/zshenv .zshenv
    ```

1. Set up tmux.

    ```bash
    cd ~
    ln -s ~/Workspace/dotfiles/tmux.conf .tmux.conf
    ```

1. Set up Emacs.

    ```bash
    cd ~
    ln -s ~/Workspace/dotfiles/emacs.d .emacs.d
    cp -a ~/Workspace/dotfiles/templates/Org ~/Documents
    ```
    
    For details on the Emacs Org mode see the file
    `templates/Org/README-Org.md`.

1. Set up Visual Studio Code.

    ```bash
    cd ~/.config/Code/User
    ln -s ~/Workspace/dotfiles/config/Code/User/keybindings.json
    ln -s ~/Workspace/dotfiles/config/Code/User/settings.json
    ```
    
    MacOS:
    
    ```bash
    cd ~/Library/Application\ Support/Code/User
    ln -s ~/Workspace/dotfiles/config/Code/User/keybindings-macos.json keybindings.json
    ln -s ~/Workspace/dotfiles/config/Code/User/settings.json
    ```

1. Set up the micro text editor (https://github.com/zyedidia/micro).

    ```bash
    cd ~/.config/micro
    ln -s ~/Workspace/dotfiles/config/micro/bindings.json
    ln -s ~/Workspace/dotfiles/config/micro/settings.json
    ```

1. Set up the alacritty terminal emulator (https://github.com/alacritty/alacritty).

    ```bash
    cd ~/.config/alacritty
    ln -s ~/Workspace/dotfiles/config/alacritty/alacritty.yml
    ```
