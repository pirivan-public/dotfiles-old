# Emacs org-mode
The file `emacs.d/settings/setup-org.el` defines default values for some of the
Org facilities, as follows:

* The Org mode root directory is `~/Documents/Org`. All further file and
  directory references in this document are relative to this directory.

* The default list of agenda files includes `notes.org`, `journal.org`, and any
  other `.org` files in the `agendas` directory.

* The default file to capture notes is `notes.org`, under the *Notes* header.

* The default file to capture tasks is `notes.org`, under the *Tasks* header.

* The default file to capture journal entries is `journal.org`.

* TODO states are *TODO*, *IN-PROGRESS*, *ON-HOLD*, *DONE*, and *CANCELLED*.

* New notes get a *draft* tag by default. Shortcuts are provided to
  switch between *draft* and *final* tags when required.

## Org Capture Mode
Three capture templates are available when you press `C-c c` to initiate a note
capture session: *journal*, *note*, and *todo*, as illustrated below:

    Select a capture template
    =========================
    
    [j]     journal
    [n]     note
    [t]     todo
    -------------------------
    [C]     Customize org-capture-templates
    [q]     Abort

* Journal entries are stored in the `journal.org` file. They are organized in
  a year-month-week-day hierarchy automatically.

* Note entries are stored in the `notes.org` file under the *Notes* header and
  get a timestamp with the creation time. They include a *draft* tag by
  default. This is a sample note:

    ```text
    ** This is the title of the note             :draft:
    :PROPERTIES:
    Created: [2018-05-04 Fri 09:41]
    :END:
    This is the content of the note
    ```

* TODO entries, or tasks, are stored in the `notes.org` file under the *Tasks*
  header and get a timestamp with the creation time. They include a default *B*
  priority (priorities are *A*, *B*, and *C*), and a LOGBOOK box to keep track
  of state changes. This is a sample task:
  
    ```text
    ** ON-HOLD [#B] Write the weekly report
    :LOGBOOK:
    - State "ON-HOLD"    from "IN-PROGRESS" [2018-05-04 Fri 19:44] \\
      I'm busy for now
    - State "IN-PROGRESS" from "TODO"       [2018-05-04 Fri 17:05] \\
      Better now than over the weekend.
    :END:
    Created: [2018-05-04 Fri 17:04]
    ```

## Org Agenda Views
An additional *Prefix Key* menu entry is available when your press `C-c a` to
access the agenda views menus, as illustrated below:

    Press key for an agenda command
    ----------------------------------        >   Remove restriction
    | a   Agenda for current week or day      e   Export agenda views               |
    | t   List of all TODO entries            T   Entries with special TODO kwd     |
    | m   Match a TAGS/PROP/TODO query        M   Like m, but only TODO entries     |
    | s   Search for keywords                 S   Like s, but only TODO entries     |
    | L   Timeline for current buffer         #   List stuck projects (!=configure) |
    | /   Multi-occur                         C   Configure custom agenda commands  |
    | ?   Find :FLAGGED: entries              *   Toggle sticky agenda views        |
    |                                                                               |
    | r   Items to refile: set of 1 commands                                        |
    | n   Prefix key                                                                |

Press the `r` key to display a list of items (notes and tasks) pending refiling,
that is, items living in the `notes.org` file.

Press the `n` key to access the template selector menu illustrated below:

    Press key for an agenda command:
    --------------------------------
    |t   Prefix key
    |h   Notes from Home   : set of 2 commands
    |w   Notes from Work   : set of 2 commands
    |W   Notes from Workstation  : set of 2 commands

Press on of the `h`, `w`, or `W` keys to display two lists of notes, not tasks,
associated with the corresponding selection: one of *draft* notes and another of
*final* notes.

Press the `t` key to access the template selector menu illustrated below:

    Press key for an agenda command:
    --------------------------------
    |h   Tasks from Home   : set of 2 commands
    |w   Tasks from Work   : set of 2 commands
    |W   Tasks from Workstation  : set of 2 commands

This is a similar menu as the one before, but it lists tasks instead of notes.

**Note:** The custom agenda views are defined in the file
`setup-org-agenda-views.el`. This facilitates making changes as required without
modifying the core `setup-org-agenda.el` file. 

## Refiling Notes and Tasks
New captured notes and tasks are stored in the `notes.org` file by default. The sample
`notes.org` file in the Org templates has the following header:

    #+TITLE: Notes and Tasks to Refile
    #+STARTUP: content
    #+CATEGORY: refile
    #+FILETAGS: @refile

The **FILETAGS: @refile** line adds the *@refile* tag to all entries in the
file. This means that any new note or task gets the *@refile* tag by default, in
addition to the *draft* tag discussed before. The *@refile* tag marks all notes
and tasks in `notes.org` as "pending for refiling," or transitional. These items
are expected to be moved to their final destination at some point in time,
typically, moved to one of the agenda files in the `agendas` directory.

**Note:** Press the `s` key while visiting an agenda after you make changes to,
or refile an item. This saves all Org-related modified buffers to disk.

## Template Files
The `templates/Org` directory in this repository contains sample documents that
can be used as starting points to capture notes, tasks, and journal
entries. They must be placed in the Org mode root directory. Here is the tree:

    templates/Org/
    ├── agendas
    │   ├── home.org
    │   ├── work.org
    │   └── workstation.org
    ├── journal.org
    └── notes.org

* `notes.org`: the default file to store new notes and tasks. Entries in this
  file get the *draft* tag (as set in the `setup-org.el` file) and the *@refile*
  tag, as set by the *FILETAG* header in the file.

* `journal.org`: the default file to store new journal entries.

* `agendas/{home,work,workstation}.org`: sample agenda files. These files are
  presented to you as default targets when you refile notes or tasks from
  `notes.org`.
  
